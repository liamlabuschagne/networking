#!/usr/bin/evn python3
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server = 'esmk.tk'
port = 80
server_ip = socket.gethostbyname(server)
request = "GET / HTTP/1.1\nHost: "+server+"\n\n"
s.connect((server,port))
s.send(request.encode())

complete = ""
result = s.recv(4096)
result = result.decode()
complete += result

while(len(result) > 0):
    result = s.recv(4096);
    result = result.decode()
    complete += result

print(complete)
